Start Scripts
=============

This folder contains pre-configured start scripts for Opencast Matterhorn.  If
you installed Matterhorn into `/opt/matterhorn`, use the scripts from the `opt`
folder. If you did a system wide installation (e.g. packaging), use the scripts
from the `system` folder.
